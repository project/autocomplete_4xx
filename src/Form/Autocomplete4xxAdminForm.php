<?php

declare(strict_types=1);

namespace Drupal\autocomplete_4xx\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a confirmation form before clearing out the logs.
 *
 * @internal
 */
final class Autocomplete4xxAdminForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\Prophecy\Prophecy\ProphecyInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, protected TypedConfigManagerInterface $typedConfigManager, EntityTypeManager $entity_type_manager) {
    $this->setConfigFactory($config_factory);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'autocomplete_4xx.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'autocomplete_4xx_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('autocomplete_4xx.settings');

    // Build list of content types to show.
    $options = [];
    foreach ($this->entityTypeManager->getStorage('node_type')->loadMultiple() as $type) {
      $access = $this->entityTypeManager->getAccessControlHandler('node')->createAccess($type->id(), NULL, [], TRUE);
      if ($access->isAllowed()) {
        $options[$type->id()] = $type->label();
      }
    }

    $form['include_routes'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include routes'),
      '#default_value' => $config->get('include_routes'),
      '#description' => $this->t('Wheather to include system known routes like /admin or any other routes that exists across the system'),
    ];

    $form['include_parameterized'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include parameterized routes'),
      '#default_value' => $config->get('include_parameterized'),
      '#description' => $this->t('If this option is enabled parameterized routes will also be available to search through.'),
    ];

    $form['include_unpublished'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include unpublished nodes'),
      '#default_value' => $config->get('include_unpublished'),
      '#description' => $this->t('If this option is enabled unpublished nodes going to be included.'),
    ];

    $form['content_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Which content types should be included for setting up 4xx pages?'),
      '#options' => $options,
      '#default_value' => $config->get('content_types'),
      '#description' => $this->t('By default all content types are available to search through while setting up 4xx pages. However you can shirnk this list to specified content types.'),
    ];

    if (empty($options)) {
      $form['content_types']['#disabled'] = TRUE;
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    // Get config.
    $config = $this->config('autocomplete_4xx.settings');
    if ($form_state->hasValue('include_routes')) {
      $config->set('include_routes', $form_state->getValue('include_routes'));
    }
    if ($form_state->hasValue('content_types')) {
      $config->set('content_types', $form_state->getValue('content_types'));
    }
    if ($form_state->hasValue('include_parameterized')) {
      $config->set('include_parameterized', $form_state->getValue('include_parameterized'));
    }
    if ($form_state->hasValue('include_unpublished')) {
      $config->set('include_unpublished', $form_state->getValue('include_unpublished'));
    }
    $config->save();
  }

}
