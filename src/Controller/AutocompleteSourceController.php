<?php

namespace Drupal\autocomplete_4xx\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Routing\RouteProviderInterface;

/**
 * Defines a route controller for watches autocomplete form elements.
 */
final class AutocompleteSourceController extends ControllerBase {

  /**
   * The controller constructor.
   */
  public function __construct(
    private readonly PathValidatorInterface $pathValidator,
    private readonly RouteProviderInterface $routerRouteProvider,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('path.validator'),
      $container->get('router.route_provider'),
    );
  }

  /**
   * Builds the response.
   */
  public function __invoke(Request $request) {
    $config = $this->config('autocomplete_4xx.settings');
    $results = [];
    $input = $request->query->get('q');

    // Get the typed string from the URL, if it exists.
    if (!$input) {
      return new JsonResponse($results);
    }

    $input = Xss::filter($input);

    $query = $this->entityTypeManager()->getStorage('node')->getQuery()
      ->accessCheck(TRUE)
      ->groupBy('nid')
      ->sort('created', 'DESC')
      ->condition('title', $input, 'CONTAINS');

    // Extend conditions if content types are selected.
    $list = [];
    if (!empty($config->get('content_types'))) {
      foreach ($config->get('content_types') as $content_type) {
        if (!empty($content_type)) {
          $list[] = $content_type;
        }
      }
      if (!empty($list)) {
        $query->condition('type', $list, 'IN');
      }
    }

    $ids = $query->execute();
    $nodes = $ids ? $this->entityTypeManager()->getStorage('node')->loadMultiple($ids) : [];
    foreach ($nodes as $node) {
      if ($config->get('include_unpublished')) {
        $results[] = [
          'value' => '/node/' . $node->id(),
          'label' => $node->getTitle() . ' <small>(' . $node->id() . ')</small>',
        ];
      }
      else {
        if ($node->isPublished()) {
          $results[] = [
            'value' => '/node/' . $node->id(),
            'label' => $node->getTitle() . ' <small>(' . $node->id() . ')</small>',
          ];
        }
      }
    }

    if ($config->get('include_routes')) {
      $route_name = $this->routeProvider->getAllRoutes();
      foreach ($route_name as $route) {
        if (strpos($route->getPath(), $input)) {
          if ($config->get('include_parameterized')) {
            $results[] = [
              'value' => $route->getPath(),
              'label' => $route->getPath(),
            ];
          }
          else {
            if (!strpos($route->getPath(), '{') && !strpos($route->getPath(), '}')) {
              $results[] = [
                'value' => $route->getPath(),
                'label' => $route->getPath(),
              ];
            }
          }
        }
      }
    }

    return new JsonResponse($results);
  }

}
