# Autocomplete 4xx

This module provides additional feature on admin/config/system/site-information page, so 404 and 403 fields becomes autocomplete. There are additional settings provided at admin/config/system/autocomplete_4xx, so you can play with these settings to adjust results of autocomplete search. Available options are:

 - Include routes - by default it's set to false and only node paths (represented by node titles) are considered to be potential results.
 - Include parameterized routes - this option is also set to false, as it allows to include in autocomplete results paths with {parameters}
 - Include unpublished nodes - by default unpublished nodes are not included in the list, this option provides an ability to include it
 - Which content types should be included for setting up 4xx pages? - by default all the nodes (no matter the content type) are available to sarch through, this option can narrow down results to specific content types.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/autocomplete_4xx).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/autocomplete_4xx).


## Requirements (required)

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration (required)

1. Enable the module at Administration > Extend or via drush.
2. Go to /admin/config/system/site-information
3. `Error pages` section (403 and 404) should now contain autocomplete fields instead of simple textfields.
4. By default autocomplete searches through all published nodes.
5. You can adjust search settings at `/admin/config/system/autocomplete_4xx`


## Maintainers

- Dawid Nawrot - [dawidnawrot](https://www.drupal.org/u/dawidnawrot)
